# MicSim

Hi! Welcome to MicSim project repository. MicSim is a Mic-1 architecture simulator written in python. It is specially designed for studying purpose. 

### How it works ?
It is simple, you have to:
 1. Clone the repo
 2. Execute `micsim.py`
 3. Enjoy !

### I want to know more about it....
Source code is located in `MicSim/` directory. All the components used for the Mic-1 architecture are located in `MicSim/components/` directory:
 - `ijvm.py` Contains standard IJVM constant
 - `microprogram.py` Contains IJVM implementation that use Mic-1 architecture
 - `caretaker.py` Hold all the Mic-1 architecture components (registers, ram etc..)
 - `ram.py` Contains a simple RAM implementation

### How to load code in memory ?
Simply by editing `ram.txt`. Each line corresponding to a byte entry starting from address 0x0 to whatever. Each line can be an **IJVM opcode** or a random **byte**. Here is an example of how to add two numbers:

   

>  BIPUSH<br />
>     12<br />
>     BIPUSH<br />
>     5<br />
>     IADD<br />

Kind of output:

    ---------- Ram Before Execution ----------
    0    : 0x10  (16)
    1    : 0xc   (12)
    2    : 0x10  (16)
    3    : 0x5   (5)
    4    : 0x60  (96)
    ------------------------------------------
    ---------- Ram After Execution ----------
    0    : 0x10  (16)
    1    : 0xc   (12)
    2    : 0x10  (16)
    3    : 0x5   (5)
    4    : 0x60  (96)
    4099 : 0x0   (0)
    4098 : 0x0   (0)
    4097 : 0x0   (0)
    4096 : 0x11  (17)
    4103 : 0x0   (0)
    4102 : 0x0   (0)
    4101 : 0x0   (0)
    4100 : 0x5   (5)
    -----------------------------------------



