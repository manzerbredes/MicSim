#!/usr/bin/python

from components.caretaker import Caretaker
import unittest
from random import randint

class CaretakerTest(unittest.TestCase):
	
	def setUp(self):
		"""
			Init test
		"""
		self.c=Caretaker(1000)
	
	def test___getitem__(self):
		"""
			Test if getitem operation follow Mic-1 rules
		"""
		for toWrite in range(0,127): # Only 7 bit for signed MBR (2^7=127)
			self.c["MBR"]=-toWrite
			self.assertEqual(self.c["MBRU"],toWrite,"Tested with {}".format(-toWrite))
			self.assertEqual(self.c["MBR"],-toWrite,"Tested with {}".format(-toWrite))
			
		for toWrite in range(0,255): # Only 2^8 value for unsigned
			self.c["MBR"]=toWrite
			self.assertEqual(self.c["MBRU"],toWrite,"Tested with {}".format(toWrite))
			if toWrite>127: # We enter in the zone of negative number at 127
				self.assertEqual(self.c["MBR"],-((toWrite-1)^0xFF),"Tested with {}".format(toWrite)) # Check if we get the reverse complement

		with self.assertRaises(KeyError): # Check it returns a KeyError
			self.c["kjhkjhkoih"+str(randint(0,7698))]
			
		# Check outside bound values
		with self.assertRaises(Exception):
			self.c["MBR"]=-128	
		with self.assertRaises(Exception):
			self.c["MBR"]=256	
		with self.assertRaises(Exception):
			self.c["PC"]=2**32
		with self.assertRaises(Exception):
			self.c["PC"]=-(2**31)
					
	def test___setitem__(self): # TODO: improve tests
		"""
			Test if getitem operation follow Mic-1 rules
		"""
		try:
			self.c["RAM"]="Test"
		except Exception:
			self.fail("Failed to assign RAM to caretaker")


if __name__ == "__main__":
	unittest.main()
