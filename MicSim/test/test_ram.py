
from components.ram import Ram
import unittest
from random import randint


class RamTest(unittest.TestCase):
	
	def setUp(self):
		"""
			Init test
		"""
		self.caretaker=dict({"MDR":0,"MAR":0,"MBR":0,"PC":0})
		self.ramSize=1000*4 # I suppose ram size should be a multiple of 4 to guaranty test validity

	def test_write(self):
		"""
			Test write method
		"""
		# Test write action
		for i in range(0,32): # Test for n number
			toWrite=randint(0,2**i) # Pick a random number to write
			self.caretaker["MDR"]=toWrite
			self.caretaker["MAR"]=randint(0,self.ramSize-1)
			ram=Ram(self.caretaker,self.ramSize)
			ram.write() # Write a random number at address 0
			data=ram.getData() # Dump ram
			##### Test if everything is written using big endian model #####
			self.assertEqual((toWrite>>24)&0xFF,data[self.caretaker["MAR"]])
			self.assertEqual((toWrite>>16)&0xFF,data[self.caretaker["MAR"]+1])
			self.assertEqual((toWrite>>8)&0xFF,data[self.caretaker["MAR"]+2])
			self.assertEqual(toWrite&0xFF,data[self.caretaker["MAR"]+3])
			
		# Test error is raise when writing out of memory
		self.caretaker["MDR"]=randint(0,2**i)
		self.caretaker["MAR"]=1000 # Write out of memory (positive address)
		ram=Ram(self.caretaker,1000)
		with self.assertRaises(Exception):
			ram.write()
		self.caretaker["MDR"]=randint(0,2**i)
		self.caretaker["MAR"]=-1000 # Write out of memory (negative address)
		ram=Ram(self.caretaker,1000)
		with self.assertRaises(Exception):
			ram.write()
		# Try to write that cannot fit in a int
		self.caretaker["MDR"]=2**32
		self.caretaker["MAR"]=0
		ram=Ram(self.caretaker,1000)
		with self.assertRaises(Exception):
			ram.write()
		
	def test_read(self):
		"""
			Test read method
		"""
		ram=Ram(self.caretaker,self.ramSize)
		data=dict()
		toWrite=randint(0,256-1)
		for i in range(0,self.ramSize): # Write in memory
			data[i]=toWrite # Write the random byte
		ram.setData(data)
		for i in range(0,int(self.ramSize/4)): # Read and check if its what we wrote
			self.caretaker["MAR"]=i*4
			data=ram.read() # Read start at 0 addr
			self.assertEqual(toWrite,(data>>24)&0xFF)
			self.assertEqual(toWrite,(data>>16)&0xFF)
			self.assertEqual(toWrite,(data>>8)&0xFF)
			self.assertEqual(toWrite,data&0xFF)
		# Try to read outside of the memory
		with self.assertRaises(Exception):
			self.caretaker["MAR"]=self.ramSize
			ram.read()
		with self.assertRaises(Exception):
			self.caretaker["MAR"]=-1*randint(1,self.ramSize-1)
			ram.read()
			
	def test_fetch(self):
		"""
			Test fetch method
		"""
		for q in range(0,1999): # For fun
			# Test classical fetch
			ram=Ram(self.caretaker,self.ramSize)
			toWrite=randint(0,256-1)
			data=dict()
			for i in range(0,self.ramSize):
				data[i]=toWrite
			ram.setData(data)
			for i in range(0,self.ramSize):
				self.caretaker["PC"]=i
				self.assertEqual(toWrite,ram.fetch())
			# Test fetch outside of memory
			with self.assertRaises(Exception):
				self.caretaker["PC"]=self.ramSize
				ram.fetch()
			with self.assertRaises(Exception):
				self.caretaker["PC"]=-1*randint(1,self.ramSize-1)
				ram.fetch()
		
if __name__ == "__main__":
	unittest.main()
