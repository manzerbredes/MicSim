#!/usr/bin/python

from components.microprogram import Microprogram
from components.ram import Ram
from components.caretaker import Caretaker

def dump(ram,title): # Simple Helper function
	data=ram.getData()
	print("---------- "+title+" ----------")
	for addr,value in data.items():
		print("%-5s: 0x%-3x (%s)"%(addr,value,value))
	print("-"*(22+len(title)))		


c=Caretaker(5000) 					   # Init components ram size in byte
c["RAM"].loadRamFile("./ram.txt")	   # Load Ram from file

mic=Microprogram(c) 				   # Create microprogram
dump(c["RAM"], "Ram Before Execution") # Dump ram before execution
mic.run(800, 1024) 					   # Run the microprogram with run(constantPoolLocation,stackLocation)
dump(c["RAM"],"Ram After Execution")   # Dump ram after execution





