#!/usr/bin/python

from components.ram import Ram

class Caretaker:
	
	def __init__(self,ramSize):
		self.objects=dict() # Create empty objects pool
		## Add registers to pool ##
		for reg in ["MAR","MDR", "PC", "MBR", "SP","LV","CPP","TOS","OPC","H"]:
			self.objects[reg]=0
		self.objects["RAM"]=Ram(self,ramSize) # Create ram
	
	def __getitem__(self,key):
		if key=="MBRU": # If we ask for unsigned
			return(abs(self.objects["MBR"]))
		elif key== "MBR": # If we ask for signed
			if self.objects[key] < 0: # If its already signed
				return(self.objects[key])
			elif self.objects[key]>>7==1: # Otherwise compute its python version
				return(-((self.objects[key]-1)^0xFF)) # Reverse 2 complements to get an unsigned number then negate it
			else: # Otherwise it is positive
				return(self.objects[key])
		return(self.objects[key])

	def __setitem__(self,key,value):
		if key!="RAM":
			if ((-(2**7-1))>value or value>(2**8)-1) and (key=="MBR" or key=="MBRU"):
				raise RuntimeError("Value {} cannot fit in {} register (-(2^7-1) minimum value and (2^7-1) maximum value)".format(value,key))
			elif (-(2**31-1)>value or value>(2**32)-1) and (key!="MBR" and key!="MBRU"):
				raise RuntimeError("Value {} cannot fit in {} register (2^32-1 maximum value)".format(value,key))
		self.objects[key]=value
		
	def items(self):
		return(self.objects.items())	
	
