from components.ijvm import ijvm

class Ram:
	
	def __init__(self,components,size):
		self.data=dict()
		self.lastAddr=size-1
		self.c=components
	
	def loadRamFile(self,filepath):
		"""
			Load a Ram file into self.data
		"""
		self.data=dict()
		f=open(filepath,"r")
		addr=0
		for line in f.readlines():
			line=line.rstrip() # remove \n
			if line in ijvm:
				self.data[addr]=int(ijvm[line])
			else:
				try:
					value=int(line,0)
				except:
					raise ValueError("Invalide RAM entry: Address {} value {}".format(addr,line))
				if value>255:
					raise ValueError("Ram contain values that does not fit in a byte: value {} at address {}".format(value,addr)) 
				self.data[addr]=value
			addr+=1
		f.close()
		
	def write(self):
		"""
			Write data to memory based Mic-1 architecture
		"""
		addr=self.c["MAR"] # Fetch address
		if addr>self.lastAddr or addr<0:
			raise ValueError("You get out of the ram by trying to set a value at address {}, max address is {}".format(addr,self.lastAddr))
		elif self.c["MDR"] >=2**32:
			raise ValueError("You try to write a the value {} at address {} but this value does not fit in a int".format(self.c["MDR"],addr))
		#### Split bytes and write ####
		self.data[addr+3]=self.c["MDR"] & 0xFF
		self.data[addr+2]=self.c["MDR"]>>8 & 0xFF
		self.data[addr+1]=self.c["MDR"]>>16 & 0xFF
		self.data[addr]=self.c["MDR"]>>24 & 0xFF
		###############################

	
	def read(self):
		"""
			Read data from memory based Mic-1 architecture
		"""
		addr=self.c["MAR"] # Fetch address
		if addr>self.lastAddr or addr<0:
			raise ValueError("You get out of the ram by trying to get value at address {}, max address is {}".format(addr,self.lastAddr))
		elif not(addr in self.data): 
			return(0)
			
		#### Combine bytes ####
		value=self.data[addr]<<24|(self.data[addr+1]<<16)|(self.data[addr+2]<<8)|(self.data[addr+3])
		#######################
		return(value)
		
	def fetch(self):
		"""
			Fetch next byte from memory based Mic-1 architecture
		"""
		addr=self.c["PC"]
		if addr>self.lastAddr or addr<0:
			raise ValueError("You get out of the ram by trying to get value at address {}, max address is {}".format(addr,self.lastAddr))
		elif not(addr in self.data): 
			return(0)
		return(self.data[addr])
		
	def getData(self):
		"""
			Get RAM data (usefull for unit tests)
		"""
		return(self.data)
		
	def setData(self,data):
		"""
			Set RAM data (usefull for unit tests)
		"""
		self.data=data

